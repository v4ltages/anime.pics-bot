# Akasuki
Discord Bot written in Discord.js using Node.js. Here to assist you in finding inspiration from other's work on https://anime.pics

The name Akasuki means bright helper. 

[Want to invite me?](https://discord.com/oauth2/authorize?client_id=764535858035425290&permissions=2147483648&scope=bot)


### Prerequisites

* Node.js 16.6.0 or newer
* NPM dependencies: dotenv, discord.js, @discordjs/builders, @discordjs/rest, and discord-api-types, axios
* Discord Token for .env
* Discord Client ID for .env

### Installation

A step by step guide that will tell you how to get the development environment up and running.

Please note that the .env file contains the DISCORD_TOKEN and CLIENTID

```
$ git clone https://gitlab.com/v4ltages/akasuki.git
$ cd ./akasuki
$ npm install dotenv discord.js @discordjs/builders @discordjs/rest and discord-api-types axios
$ npm update
$ echo > .env
```

## Slash commands

- contests opt: latest, previous or num: 0-20
- feed num: offset
- lookup user: Look up a user with their username, posts: All posts from a User with their username, post: Specific post via its ID
- search users: term or posts: term

## License

This project is licensed under the GNU General Public License v3.0.

[Click here to view it.](LICENSE)

