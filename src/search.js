const { MessageEmbed } = require('discord.js');
const axios = require('axios');
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('search')
        .setDescription('Search for users or posts from anime.pics')
        .addStringOption(option => option.setName('users').setDescription('Look up a user with a term'))
        .addStringOption(option => option.setName('posts').setDescription('Look for posts with a term')),
    async execute(interaction) {
        const postSearchTerm = interaction.options.getString('posts')
        const userSearchTerm = interaction.options.getString('users')
        if ( userSearchTerm ) {
            const rApi = await axios.post('https://gateway.anime.pics/query/user', { term:`${userSearchTerm}` })
            const totalResults = rApi.data.users
            const embed = new MessageEmbed()
                .setTitle(`Search results for Users`)
                for (const field of rApi.data.users) {
                    embed.addFields({ name: `${field.username} - ${field.id}`, value: `👀 Total followers: ${field.follower} | 📃 Total posts: ${field.post_count} | [Link](https://anime.pics/profile/${field.username}) \n ${field.description}` })
                }
                embed.setFooter(`Total results: ${totalResults.length}`)
            return interaction.reply({ embeds: [embed], ephemeral: true });
        }
        else if ( postSearchTerm ) {
            const rApi = await axios.post('https://gateway.anime.pics/query/posts', { term:`${postSearchTerm}` })
            const totalResults = rApi.data.users
            const embed = new MessageEmbed()
            .setTitle(`Search results for Posts`)
            for (const field of rApi.data.posts) {
                embed.addFields({ name: `${field.title} - ${field.id}`, value: `❤️ Likes: ${field.likes} | 👀 Views: ${field.views} | [Link](https://anime.pics/p/${field.id}) \n ${field.description}` })
            }
            embed.setFooter(`Total results: ${totalResults.length}`)
            return interaction.reply({ embeds: [embed], ephemeral: true});
        }
        else {
            return interaction.reply({ content: 'Nyaa... i have nothing to search for. Do /search users: or /search posts:', ephemeral: true });
        }
    },
};