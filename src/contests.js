const { MessageEmbed } = require('discord.js');
const axios = require('axios');
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('contests')
        .setDescription('Look at what contests are currently on')
        .addStringOption(option => option.setName('opt').setDescription('Latest, Previous')).addNumberOption(option => option.setName('num').setDescription('Number from 0-20')),
    async execute(interaction) {
        const opt = interaction.options.getString('opt')
        const num = interaction.options.getNumber('num')
        if (opt) {
            switch (opt.toLowerCase()) {
                case 'latest': case 'l': {
                    const rApi = await axios.get('https://gateway.anime.pics/contests/')
                        const embed = new MessageEmbed()
                        .setTitle(rApi.data[0].title)
                        .setImage(`https://content.anime.pics/${rApi.data[0].banner}`)
                        .setDescription(`${rApi.data[0].description} \n\n 📃 Entries: ${rApi.data[0].entry_count} \n 🔒 Status: ${rApi.data[0].closed ? 'Closed' : 'Open'}`)
                        .setFooter(`Posted by ${rApi.data[0].creator_username}`);
                    return interaction.reply({ embeds: [embed]});
                }
                case 'previous': case 'p': case 'prev': {
                    const rApi = await axios.get('https://gateway.anime.pics/contests/')
                        const embed = new MessageEmbed()
                        .setTitle(rApi.data[1].title)
                        .setImage(`https://content.anime.pics/${rApi.data[1].banner}`)
                        .setDescription(`${rApi.data[1].description} \n\n 📃 Entries: ${rApi.data[1].entry_count} \n 🔒 Status: ${rApi.data[1].closed ? 'Closed' : 'Open'}`)
                        .setFooter(`Posted by ${rApi.data[1].creator_username}`);
                    return interaction.reply({ embeds: [embed]});
                }
            }
        }
        else if (num) {
            if (num < 21) {
                const rApi = await axios.get('https://gateway.anime.pics/contests/')
                    const embed = new MessageEmbed()
                    .setTitle(rApi.data[num].title)
                    .setImage(`https://content.anime.pics/${rApi.data[num].banner}`)
                    .setDescription(`${rApi.data[num].description} \n\n 📃 Entries: ${rApi.data[num].entry_count} \n 🔒 Status: ${rApi.data[num].closed ? 'Closed' : 'Open'}`)
                    .setFooter(`Posted by ${rApi.data[num].creator_username}`);
                return interaction.reply({ embeds: [embed]});
            }
            else {
                return interaction.reply({ content: 'The number given is not in the spesified range of 0-20.', ephemeral: true });
            }
        }
        const rApi = await axios.get('https://gateway.anime.pics/contests/')
        const embed = new MessageEmbed()
        .setTitle(rApi.data[0].title)
        .setImage(`https://content.anime.pics/${rApi.data[0].banner}`)
        .setDescription(`${rApi.data[0].description} \n\n 📃 Entries: ${rApi.data[0].entry_count} \n 🔒 Status: ${rApi.data[0].closed ? 'Closed' : 'Open'}`)
        .setFooter(`Posted by ${rApi.data[0].creator_username}`);
        return interaction.reply({ embeds: [embed]});
    },
};
