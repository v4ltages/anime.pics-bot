const { MessageEmbed } = require('discord.js');
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('about')
        .setDescription('What about me?'),
    async execute(interaction) {
        const embed = new MessageEmbed()
        .setTitle('Hiya 👋')
        .setDescription('This was made with the intention of learning more about programming and growing my knowledge. I had this idea and wanted to see if i can do it so people have a easier time looking for inspiration. Liz3 helped out aswell ❤️ - Voltages')
        .addFields({ name: 'My info', value: '[My Website](https://voltages.me/) \n [Buy me a tea](https://ko-fi.com/voltages)' })
        .addFields({ name: 'Support anime.pics ❤️', value: '[Show some love to Liz3 for making anime.pics](https://anime.pics/donate)' })
        .setFooter('Made with love <3');
        return interaction.reply({ embeds: [embed]});
    },
};